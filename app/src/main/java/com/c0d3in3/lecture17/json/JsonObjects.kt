package com.c0d3in3.lecture17.json

const val JSON_PAGE_SIZE = 2

const val JSON_OBJECT_PAGE_2 = "{\n" +
        "   \"page\":2,\n" +
        "   \"per_page\":6,\n" +
        "   \"total\":12,\n" +
        "   \"total_pages\":2,\n" +
        "   \"data\":[\n" +
        "      {\n" +
        "         \"id\":7,\n" +
        "         \"email\":\"michael.lawson@reqres.in\",\n" +
        "         \"first_name\":\"Michael\",\n" +
        "         \"last_name\":\"Lawson\",\n" +
        "         \"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/follettkyle/128.jpg\"\n" +
        "      },\n" +
        "      {\n" +
        "         \"id\":8,\n" +
        "         \"email\":\"lindsay.ferguson@reqres.in\",\n" +
        "         \"first_name\":\"Lindsay\",\n" +
        "         \"last_name\":\"Ferguson\",\n" +
        "         \"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/araa3185/128.jpg\"\n" +
        "      },\n" +
        "      {\n" +
        "         \"id\":9,\n" +
        "         \"email\":\"tobias.funke@reqres.in\",\n" +
        "         \"first_name\":\"Tobias\",\n" +
        "         \"last_name\":\"Funke\",\n" +
        "         \"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/vivekprvr/128.jpg\"\n" +
        "      },\n" +
        "      {\n" +
        "         \"id\":10,\n" +
        "         \"email\":\"byron.fields@reqres.in\",\n" +
        "         \"first_name\":\"Byron\",\n" +
        "         \"last_name\":\"Fields\",\n" +
        "         \"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/russoedu/128.jpg\"\n" +
        "      },\n" +
        "      {\n" +
        "         \"id\":11,\n" +
        "         \"email\":\"george.edwards@reqres.in\",\n" +
        "         \"first_name\":\"George\",\n" +
        "         \"last_name\":\"Edwards\",\n" +
        "         \"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/mrmoiree/128.jpg\"\n" +
        "      },\n" +
        "      {\n" +
        "         \"id\":12,\n" +
        "         \"email\":\"rachel.howell@reqres.in\",\n" +
        "         \"first_name\":\"Rachel\",\n" +
        "         \"last_name\":\"Howell\",\n" +
        "         \"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/hebertialmeida/128.jpg\"\n" +
        "      }\n" +
        "   ],\n" +
        "   \"ad\":{\n" +
        "      \"company\":\"StatusCode Weekly2\",\n" +
        "      \"url\":\"http://statuscode.org/\",\n" +
        "      \"text\":\"A weekly newsletter focusing on software development, infrastructure, the server, performance, and the stack end of things.\"\n" +
        "   }\n" +
        "}"

const val JSON_OBJECT_PAGE_1 = "{\n" +
        "   \"page\":1,\n" +
        "   \"per_page\":6,\n" +
        "   \"total\":12,\n" +
        "   \"total_pages\":2,\n" +
        "   \"data\":[\n" +
        "      {\n" +
        "         \"id\":1,\n" +
        "         \"email\":\"george.bluth@reqres.in\",\n" +
        "         \"first_name\":\"George\",\n" +
        "         \"last_name\":\"Bluth\",\n" +
        "         \"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/calebogden/128.jpg\"\n" +
        "      },\n" +
        "      {\n" +
        "         \"id\":2,\n" +
        "         \"email\":\"janet.weaver@reqres.in\",\n" +
        "         \"first_name\":\"Janet\",\n" +
        "         \"last_name\":\"Weaver\",\n" +
        "         \"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/josephstein/128.jpg\"\n" +
        "      },\n" +
        "      {\n" +
        "         \"id\":3,\n" +
        "         \"email\":\"emma.wong@reqres.in\",\n" +
        "         \"first_name\":\"Emma\",\n" +
        "         \"last_name\":\"Wong\",\n" +
        "         \"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/olegpogodaev/128.jpg\"\n" +
        "      },\n" +
        "      {\n" +
        "         \"id\":4,\n" +
        "         \"email\":\"eve.holt@reqres.in\",\n" +
        "         \"first_name\":\"Eve\",\n" +
        "         \"last_name\":\"Holt\",\n" +
        "         \"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/marcoramires/128.jpg\"\n" +
        "      },\n" +
        "      {\n" +
        "         \"id\":5,\n" +
        "         \"email\":\"charles.morris@reqres.in\",\n" +
        "         \"first_name\":\"Charles\",\n" +
        "         \"last_name\":\"Morris\",\n" +
        "         \"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/stephenmoon/128.jpg\"\n" +
        "      },\n" +
        "      {\n" +
        "         \"id\":6,\n" +
        "         \"email\":\"tracey.ramos@reqres.in\",\n" +
        "         \"first_name\":\"Tracey\",\n" +
        "         \"last_name\":\"Ramos\",\n" +
        "         \"avatar\":\"https://s3.amazonaws.com/uifaces/faces/twitter/bigmancho/128.jpg\"\n" +
        "      }\n" +
        "   ],\n" +
        "   \"ad\":{\n" +
        "      \"company\":\"StatusCode Weekly1\",\n" +
        "      \"url\":\"http://statuscode.org/\",\n" +
        "      \"text\":\"A weekly newsletter focusing on software development, infrastructure, the server, performance, and the stack end of things.\"\n" +
        "   }\n" +
        "}"