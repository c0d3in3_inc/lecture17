package com.c0d3in3.lecture17.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.recyclerview.widget.LinearLayoutManager
import com.c0d3in3.lecture17.R
import com.c0d3in3.lecture17.adapter.UsersAdapter
import com.c0d3in3.lecture17.json.JSON_OBJECT_PAGE_1
import com.c0d3in3.lecture17.json.JSON_OBJECT_PAGE_2
import com.c0d3in3.lecture17.model.*
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    //SWIPE DOWN TO LOAD ANOTHER PAGE and vice versa
    //SWIPE DOWN TO LOAD ANOTHER PAGE and vice versa
    //SWIPE DOWN TO LOAD ANOTHER PAGE and vice versa
    private var currentPage = 0
    private val pageData = PageData()
    private val jsonPages = arrayListOf<JSONObject>()
    private val dataList = arrayListOf<AllData>()
    private lateinit var adapter : UsersAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        jsonPages.add(JSONObject(JSON_OBJECT_PAGE_1))
        jsonPages.add(JSONObject(JSON_OBJECT_PAGE_2))
        init()
    }

    private fun init(){

        usersRecyclerView.layoutManager = LinearLayoutManager(this)
        adapter = UsersAdapter(dataList, this)

        parsePage(currentPage)

        swipeLayout.setOnRefreshListener {
            swipeLayout.isRefreshing = true
            dataList.clear()
            adapter.notifyDataSetChanged()
            if(currentPage == 0){
                currentPage++
            }
            else currentPage--
            val handler= Handler()
            handler.postDelayed({parsePage(currentPage)},2000)
        }

    }

    private fun parsePage(page : Int) {
        if(!jsonPages[page].isNull("page")) pageData.page = jsonPages[page].getInt("page")
        if(!jsonPages[page].isNull("per_page")) pageData.perPage = jsonPages[page].getInt("per_page")
        if(!jsonPages[page].isNull("total")) pageData.total = jsonPages[page].getInt("total")
        if(!jsonPages[page].isNull("total_pages")) pageData.totalPages = jsonPages[page].getInt("total_pages")
        if(!jsonPages[page].isNull("data")){
            val data = jsonPages[page].getJSONArray("data")
            for(i in 0 until data.length()){
                val jObject = data.getJSONObject(i)
                val userData = UserData()
                if(!jObject.isNull("id")) userData.id = jObject.getInt("id")
                if(!jObject.isNull("email")) userData.email = jObject.getString("email")
                if(!jObject.isNull("first_name")) userData.firstName = jObject.getString("first_name")
                if(!jObject.isNull("last_name")) userData.lastName = jObject.getString("last_name")
                if(!jObject.isNull("avatar")) userData.avatar = jObject.getString("avatar")
                dataList.add(userData)
            }
        }
        if(jsonPages[page].has("ad")){
            val ad = jsonPages[page].getJSONObject("ad")
            val adData = AdData()
            if(!ad.isNull("company")) adData.company = ad.getString("company")
            if(!ad.isNull("url")) adData.url = ad.getString("url")
            if(!ad.isNull("text")) adData.text = ad.getString("text")
            dataList.add(adData)
        }
        usersRecyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
        swipeLayout.isRefreshing = false
        labelTextView.text = "Page ${pageData.page}"
        currentPage = page
    }


}
