package com.c0d3in3.lecture17.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.c0d3in3.lecture17.R
import com.c0d3in3.lecture17.model.AdData
import com.c0d3in3.lecture17.model.AllData
import com.c0d3in3.lecture17.model.UserData
import kotlinx.android.synthetic.main.advertisement_item_layout.view.*
import kotlinx.android.synthetic.main.user_item_layout.view.*

class UsersAdapter(private val items: ArrayList<AllData>, private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object{
        const val USER_DATA = 1
        const val AD_DATA = 2
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if(viewType == USER_DATA) UserViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.user_item_layout,parent,false))
        else AdViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.advertisement_item_layout, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int {
        return if(items[position] is UserData) USER_DATA else AD_DATA
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder is UserViewHolder) holder.onBind()
        else if(holder is AdViewHolder) holder.onBind()
    }

    inner class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun onBind(){
            val userModel = items[adapterPosition] as UserData
            itemView.firstNameTextView.text = "First name: ${userModel.firstName}"
            itemView.lastNameTextView.text = "Last name: ${userModel.lastName}"
            itemView.emailTextView.text = userModel.email
            Glide.with(context).load(userModel.avatar).placeholder(R.mipmap.ic_launcher).into(itemView.userImage)
        }
    }

    inner class AdViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun onBind(){
            val advModel = items[adapterPosition] as AdData
            itemView.companyTextView.text = advModel.company
            itemView.textTextView.text = advModel.text
            itemView.urlTextView.text = advModel.url
       }
    }
}