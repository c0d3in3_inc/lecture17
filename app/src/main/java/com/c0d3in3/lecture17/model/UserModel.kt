package com.c0d3in3.lecture17.model

class PageData {
    var page : Int? = 0
    var perPage : Int? = 0
    var total : Int? = 0
    var totalPages : Int? = 0

}

class UserData : AllData{
    var id : Int? = 0
    var email : String? = ""
    var firstName : String? = "N\\A"
    var lastName : String? = "N\\A"
    var avatar : String? = null
}

class AdData : AllData{
    var company : String? = "N\\A"
    var url : String? = "N\\A"
    var text : String? = "N\\A"
}

interface AllData